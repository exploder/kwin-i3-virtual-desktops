# KWin-i3-virtual-desktops

**NOTE:** The current version only supports Plasma 6.
For a Plasma 5 compatible version, see commit [`a48cef5b`](https://gitlab.com/exploder/kwin-i3-virtual-desktops/-/tree/a48cef5b1a9040a7777f21704f908d6ba33d886f).

i3-like virtual desktop management for KWin

This KWin script basically copies the virtual desktop management and window moving functionalities from i3.

## Features

- use keyboard shortcuts (should be Alt+\<number\> by default, configurable) to move between virtual desktops
- use keyboard shortcuts (should be Alt+Shift+\<number\> by default) to move the active window between desktops
- automatically create and remove virtual desktops when needed

## Installation

1. Clone this repo.

```
git clone https://gitlab.com/exploder/kwin-i3-virtual-desktops.git
```

2. Install the script using `kpackagetool6`.

```
packagetool6 --type=KWin/Script -i kwin-i3-virtual-desktops
```

3. Open the script manager

```
kcmshell6 kcm_kwin_scripts
```

4. Enable the script from the list.

5. Open System settings, navigate to *shortcuts -> KWin* and edit/add shortcuts to your liking.
The shortcuts added by this script are named as *"Move active window to desktop \<number\>"* and *"Switch to desktop \<number\>"*.

## Removal

You can remove plugins from the manager (`kcmshell6 kcm_kwin_scripts`), but the shortcuts will stay.
You'll need to log out of Plasma and remove them from `.config/kglobalshortcutsrc` for them to disappear from System Settings.
This is some kind of settings limitation.
You can also remove all KWin shortcuts in System Settings, but in case you have something configured there, those settings will be reset to their default values.
