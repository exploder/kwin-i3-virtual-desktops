function switchToDesktop(desktopNum) {
    while (workspace.desktops.length < desktopNum) {
        workspace.createDesktop(workspace.desktops.length, "");
    }

    var largestNum = desktopNum;

    for (let window of workspace.stackingOrder) {
        for (let desktop of window.desktops) {
            if (desktop.x11DesktopNumber > largestNum) {
                largestNum = desktop.x11DesktopNumber;
            }
        }
    }

    var toDelete = []

    for (let desktop of workspace.desktops) {
        if (desktop.x11DesktopNumber == desktopNum) {
            workspace.currentDesktop = desktop;
        }
        if (desktop.x11DesktopNumber > largestNum) {
            toDelete.push(desktop);
        }
    }

    for (let desktop of toDelete) {
        workspace.removeDesktop(desktop);
    }
}

function sendActiveWindowToDesktop(desktopNum) {
    while (workspace.desktops.length < desktopNum) {
        workspace.createDesktop(workspace.desktops.length, "");
    }
    var destination = null;
    for (let desktop of workspace.desktops) {
        if (desktop.x11DesktopNumber == desktopNum) {
            destination = desktop;
            break;
        }
    }
    if (destination === null) {
        console.log("Could not find destination desktop");
        return;
    }
    workspace.activeWindow.desktops = [destination];
}

registerShortcut("SwitchTo1", "Switch to desktop 1", "Alt+1", () => switchToDesktop(1));
registerShortcut("SwitchTo2", "Switch to desktop 2", "Alt+2", () => switchToDesktop(2));
registerShortcut("SwitchTo3", "Switch to desktop 3", "Alt+3", () => switchToDesktop(3));
registerShortcut("SwitchTo4", "Switch to desktop 4", "Alt+4", () => switchToDesktop(4));
registerShortcut("SwitchTo5", "Switch to desktop 5", "Alt+5", () => switchToDesktop(5));
registerShortcut("SwitchTo6", "Switch to desktop 6", "Alt+6", () => switchToDesktop(6));
registerShortcut("SwitchTo7", "Switch to desktop 7", "Alt+7", () => switchToDesktop(7));
registerShortcut("SwitchTo8", "Switch to desktop 8", "Alt+8", () => switchToDesktop(8));
registerShortcut("SwitchTo9", "Switch to desktop 9", "Alt+9", () => switchToDesktop(9));
registerShortcut("SwitchTo10", "Switch to desktop 10", "Alt+0", () => switchToDesktop(10));

registerShortcut("MoveTo1", "Move active window to desktop 1", "Alt+!", () => sendActiveWindowToDesktop(1));
registerShortcut("MoveTo2", "Move active window to desktop 2", "Alt+\"", () => sendActiveWindowToDesktop(2));
registerShortcut("MoveTo3", "Move active window to desktop 3", "Alt+#", () => sendActiveWindowToDesktop(3));
registerShortcut("MoveTo4", "Move active window to desktop 4", "Alt+¤", () => sendActiveWindowToDesktop(4));
registerShortcut("MoveTo5", "Move active window to desktop 5", "Alt+%", () => sendActiveWindowToDesktop(5));
registerShortcut("MoveTo6", "Move active window to desktop 6", "Alt+&", () => sendActiveWindowToDesktop(6));
registerShortcut("MoveTo7", "Move active window to desktop 7", "Alt+/", () => sendActiveWindowToDesktop(7));
registerShortcut("MoveTo8", "Move active window to desktop 8", "Alt+(", () => sendActiveWindowToDesktop(8));
registerShortcut("MoveTo9", "Move active window to desktop 9", "Alt+)", () => sendActiveWindowToDesktop(9));
registerShortcut("MoveTo10", "Move active window to desktop 10", "Alt+=", () => sendActiveWindowToDesktop(10));
